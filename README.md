# Miscellaneous Syncing tools

This repo originally started life as a script to downloading Manga chapters from mangahere.com so that I could read them on my phone, eventually I required a way of packaging them and getting them on my phone as well.

Sadly, because Google are asshats and removed USB mass storage support from Android, the only *reliable* way of getting files onto it from Mac OSX is using `adb push` from the Android SDK. The Android File Transfer tool (and Sony Bridge for Mac) cannot transfer files consistently in good time.

So `adb-transfer` is a small script to push pre-defined types of files (namely Manga, Music and Anime) onto my android devices in proper places.
